This repo contains a list of QA titles, roles, and associated responsibilities:

Makers
Makers path

Engineer
Engineer II
Senior Engineer
Staff Engineer
Senior Staff Engineer
Principal Engineer
Principal Architect
Managers
Managers path

Engineering Manager
Senior Engineering Manager
Director, Engineering
VP, Engineering
Roles
Product Engineering Lead